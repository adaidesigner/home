//
//  HomeRoom.swift
//  home
//
//  Created by Yun Zeng on 2019/1/31.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

// 房间
class Room: Codable {
    var id: String
    var name: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

extension Room: CustomStringConvertible {
    var description: String {
        let data = try! JSONEncoder().encode(self)
        return String(data: data, encoding: .utf8)!
    }
}

// 房间管理
class RoomManager: Codable {
    var rooms: [Room]
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.rooms)
    }
    
    init(rooms: [Room]) {
        self.rooms = rooms
    }
    
    public required convenience init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let rooms = try container.decode([Room].self)
        self.init(rooms: rooms)
    }
    
    func findRoom(id: String) -> Room? {
        for room in self.rooms {
            if room.id == id {
                return room
            }
        }
        return nil
    }
}




