//
//  HomeMember.swift
//  home
//
//  Created by Yun Zeng on 2018/11/25.
//  Copyright © 2018 Yun Zeng. All rights reserved.
//

import Foundation

class HomeMember: Codable {
    var id: String
    var role: String

    private enum CodingKeys: String, CodingKey {
        case id
        case role
    }

    init(id: String, role: String) {
        self.id = id
        self.role = role
    }
    
}

class HomeMemberManager: Codable {
    var members: [HomeMember]
    
    init(members: [HomeMember]) {
        self.members = members
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.members)
    }
    
    public required convenience init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let members = try container.decode([HomeMember].self)
        self.init(members: members)
    }
}


