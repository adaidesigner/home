//
//  Logger.swift
//  home
//
//  Created by Yun Zeng on 2018/11/24.
//  Copyright © 2018 Yun Zeng. All rights reserved.
//

import Foundation

class Logger {
    public enum Level: String {
        case verbose = "[trace]"
        case debug = "[debug]"
        case info = "[info]"
        case warning = "[warn]"
        case error = "[error]"
    }
    
    internal static func output(level: Level, message: @escaping @autoclosure () -> Any, file: String = #file, function: String = #function, line: Int = #line) {
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "MM-dd HH:mm:ss+SSS"
        let str = formatter.string(from: now)
        print("\(str) \(level.rawValue) \((file as NSString).lastPathComponent)(\(line)): \(message())")
    }
    
    static func Verbose(_ message: @escaping @autoclosure () -> Any, _
        file: String = #file, _ function: String = #function, line: Int = #line) {
        output(level: .verbose, message: message, file: file, function: function, line: line)
    }
    
    static func Debug(_ message: @escaping @autoclosure () -> Any, _
        file: String = #file, _ function: String = #function, line: Int = #line) {
        output(level: .debug, message: message, file: file, function: function, line: line)
    }
    
    static func Info(_ message: @escaping @autoclosure () -> Any, _
        file: String = #file, _ function: String = #function, line: Int = #line) {
        output(level: .info, message: message, file: file, function: function, line: line)
    }
    
    static func Warning(_ message: @escaping @autoclosure () -> Any, _
        file: String = #file, _ function: String = #function, line: Int = #line) {
        output(level: .warning, message: message, file: file, function: function, line: line)
    }
    
    static func Error(_ message: @escaping @autoclosure () -> Any, _
        file: String = #file, _ function: String = #function, line: Int = #line) {
        output(level: .error, message: message, file: file, function: function, line: line)
    }
}
