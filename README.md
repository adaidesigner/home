# Home V2.0

该项目属于智能家居定制项目的一部分，该项目还包括:

- [**homehub 设备接入网关, 采用Go+C开发**](https://gitee.com/adaidesigner/homehub)
- [**homemaster-driver 网关硬件设备驱动, 由C编写的linux内核模块**](https://gitee.com/adaidesigner/homemaster-driver)
- [**zigbee3.0-coordinator 网关协调器部分, 芯片采用NXP的JN5169**](https://gitee.com/adaidesigner/zigbee3.0-coordinator)
- [**jarvis 家庭控制中枢服务端部分, 采用Go编写**](https://gitee.com/adaidesigner/jarvis)
`取名来源: JARVIS(贾维斯)钢铁侠托尼的AI助理，幻视的核心软件`
- [**home 家居ios端应用程序,兼容iphone和ipad, 由swift编写 (半成品, 未完工)**](https://gitee.com/adaidesigner/home)

## 智能家居定制项目介绍网址: **https://adai.design/design**

## 智能家居手机客户端 Home V2.0
## 主界面
----
![image](Doc/home-iphone.png)

## iPad客户端
----
![image](Doc/home-ipad.png)

## 设备控制面板
----
![image](Doc/control-panel.png)

## 历史记录
- 2019-08-04 创建新工程, 主要为了兼容ipad版

----
## 智能家居定制项目介绍网址: **https://adai.design/design**
