//
//  UIStackViewExt.swift
//  home
//
//  Created by Yun Zeng on 2019/9/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

// MARK: - UIStackView定制
extension UIStackView {
    enum ViewPos {
        case top
        case middle
        case bottom
    }
    
    func customize(backgroundColor: UIColor = .clear, radiusSize: CGFloat = 0) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = backgroundColor
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)

        subView.layer.cornerRadius = radiusSize
        subView.layer.masksToBounds = true
        subView.clipsToBounds = true
    }
    
    func addCustomView(view: UIView, pos: ViewPos = .middle) {
        if pos == .middle || pos == .bottom {
            let view = UIView()
            self.addArrangedSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            view.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            view.backgroundColor = UIColor(hex: "D1D1D6")
        }
        
        if pos == .top {
            let space = UIView()
            self.addArrangedSubview(space)
            space.translatesAutoresizingMaskIntoConstraints = false
            space.heightAnchor.constraint(equalToConstant: 3).isActive = true
            space.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        }
        
        self.addArrangedSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 54).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        if pos == .bottom {
            let space = UIView()
            self.addArrangedSubview(space)
            space.translatesAutoresizingMaskIntoConstraints = false
            space.heightAnchor.constraint(equalToConstant: 3).isActive = true
            space.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        }
    }
    
    func addSpace(height: Int) {
        let view = UIView()
        self.addArrangedSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
        view.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        view.backgroundColor = UIColor.clear
    }
    
    func addCustomControl(view: UIView, height: Int = 54, pos: ViewPos = .middle) {
        if pos == .middle || pos == .bottom {
            let view = UIView()
            self.addArrangedSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            view.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            view.backgroundColor = UIColor(hex: "D1D1D6")
        }
        
        self.addArrangedSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
}

