//
//  PowerButton.swift
//  home
//
//  Created by Yun Zeng on 2018/5/24.
//  Copyright © 2018 Adai.Design. All rights reserved.
//

import UIKit

@IBDesignable class PowerButton: UIControl {
    var onTapped: (()->Void)?
    // 电源状态
    enum PowerState: Int {
        case on
        case off
    }
    var power = PowerState.off {
        didSet {
            if self.power == .on {
                self.backgroundColor = self.powerOffColor
            } else {
                self.backgroundColor = self.powerOnColor
            }
        }
    }

    static let defaultSize = 62

    private let powerOnColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    private let powerOffColor = UIColor(red: 255/255, green: 98/255, blue: 89/255, alpha: 1)

    func setup() {
        self.backgroundColor = self.powerOnColor
        self.layer.cornerRadius = CGFloat(PowerButton.defaultSize/2)
        self.layer.masksToBounds = true
        
        let icon = IconView(frame: self.frame)
        self.addSubview(icon)
        icon.backgroundColor = UIColor.clear
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.heightAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        icon.widthAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        icon.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        icon.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }

    convenience init(x: Int, y: Int, state: PowerState = .off) {
        let rect = CGRect(x: x - PowerButton.defaultSize/2, y: y, width: PowerButton.defaultSize, height: PowerButton.defaultSize)
        self.init(frame: rect)
        self.power = state
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}

extension PowerButton {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.1) {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
//        if let touch = touches.first {
//            let rate = touch.force / touch.maximumPossibleForce
//            let scaling = 1.0 + (rate / 2) + 0.3
//            UIView.animate(withDuration: 0.1, animations: {
//                self.transform = CGAffineTransform(scaleX: scaling, y: scaling)
//            })
//        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
            self.transform = .identity
        })
        self.onTapped?()
    }
    
    class IconView: UIView {
        override func draw(_ rect: CGRect) {
            let strokeColor = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
            let bezier4Path = UIBezierPath()
            bezier4Path.move(to: CGPoint(x: 21.86, y: 22.08))
            bezier4Path.addCurve(to: CGPoint(x: 17, y: 32.68), controlPoint1: CGPoint(x: 18.89, y: 24.64), controlPoint2: CGPoint(x: 17, y: 28.44))
            bezier4Path.addCurve(to: CGPoint(x: 31, y: 46.66), controlPoint1: CGPoint(x: 17, y: 40.4), controlPoint2: CGPoint(x: 23.27, y: 46.66))
            bezier4Path.addCurve(to: CGPoint(x: 45, y: 32.68), controlPoint1: CGPoint(x: 38.73, y: 46.66), controlPoint2: CGPoint(x: 45, y: 40.4))
            bezier4Path.addCurve(to: CGPoint(x: 40.17, y: 22.11), controlPoint1: CGPoint(x: 45, y: 28.45), controlPoint2: CGPoint(x: 43.13, y: 24.67))
            strokeColor.setStroke()
            bezier4Path.lineWidth = 4
            bezier4Path.lineCapStyle = .round
            bezier4Path.lineJoinStyle = .round
            bezier4Path.stroke()

            //// Bezier 5 Drawing
            let bezier5Path = UIBezierPath()
            bezier5Path.move(to: CGPoint(x: 31.06, y: 16))
            bezier5Path.addLine(to: CGPoint(x: 31.06, y: 29.5))
            strokeColor.setStroke()
            bezier5Path.lineWidth = 4
            bezier5Path.lineCapStyle = .round
            bezier5Path.lineJoinStyle = .round
            bezier5Path.stroke()
        }
    }
}

