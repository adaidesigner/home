//
//  ServiceDefault.swift
//  home
//
//  Created by Yun Zeng on 2019/1/31.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServiceDefault {
    var service: Service
    
    init?(service: Service) {
        self.service = service
    }
}

// 实现单元格显示协议
extension ServiceDefault: ServiceCellDelegate {
    var icon: UIView {
        guard  let resource = self.service.icon else {
            return UIView(frame: ServiceCellView.iconSize)
        }
        let imageView = UIImageView(frame: ServiceCellView.iconSize)
        imageView.image = UIImage(named: resource)
        return imageView
    }
    
    var room: String {
        if let r = self.service.room {
            return r.name
        }
        return "默认房间"
    }
    
    var name: String {
        if self.service.name != "" {
            return self.service.name
        }
        return "请命名"
    }
    
    var state: Service.State {
        return self.service.state
    }
    
    var desc: String {
        if self.state == .offline {
            return "离线"
        } else if self.state == .update {
            return "正在更细"
        } else {
            return "在线"
        }
    }
    
    func toggle() -> HomeAction? {
        return nil
    }
}



