//
//  ServiceZigbee.swift
//  home
//
//  Created by Yun Zeng on 2019/2/1.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServiceCoordinator {
    var service: Service
    
    var netAddr: HMCharacteristic
    var macAddr: HMCharacteristic
    var panId: HMCharacteristic
    var channel: HMCharacteristic
    var permitJoin: HMCharacteristic
    
    init?(service: Service) {
        if service.type != HMServiceType.zigbeeCoordinator.rawValue {
            return nil
        }
        
        guard let netAddr = service.findCharacteristic(type: .coordinatorNetAddr),
            let macAddr = service.findCharacteristic(type: .coordinatorMacAddr),
            let panId = service.findCharacteristic(type: .coordinatorPanId),
            let channel = service.findCharacteristic(type: .coordinatorChannel),
            let permitJoin = service.findCharacteristic(type: .coordinatorPermitJoin) else {
                return nil
        }
        self.service = service
        
        self.netAddr = netAddr
        self.macAddr = macAddr
        self.panId = panId
        self.channel = channel
        self.permitJoin = permitJoin
    }

    var permitJoinValue: Int {
        get {
            if let value = self.permitJoin.value?.toValueInt() {
                if value > 0 {
                    self.service.state = .highlight
                }
                return value
            }
            return 0
        }
    }
}

// cell 显示协议
extension ServiceCoordinator: ServiceCellDelegate {
    var icon: UIView {
        let imageView = UIImageView(frame: ServiceCellView.iconSize)
        var imgSrc = ""
        if let icon = self.service.icon {
            if self.service.state != .offline && self.permitJoinValue > 0 {
                imgSrc = "service-" + icon + "-active"
            } else {
                imgSrc = "service-" + icon + "-online"
            }
        }
        imageView.image = UIImage(named: imgSrc)
        return imageView
    }
    
    var room: String {
        if let r = self.service.room {
            return r.name
        }
        return "默认房间"
    }
    
    var name: String {
        if self.service.name != "" {
            return self.service.name
        }
        return "请命名"
    }
    
    var state: Service.State {
        return self.service.state
    }
    
    var desc: String {
        if self.permitJoinValue > 0 {
            return "入网: \(self.permitJoinValue)s"
        } else if self.service.state == .offline {
            return "离线"
        } else if self.service.state == .online {
            return "在线"
        } else if self.service.state == .update {
            return "正在更新"
        }
        return ""
    }
    
    func toggle() -> HomeAction? {
        return nil
    }
}

// 入网许可设置
extension ServiceCoordinator {
    func setPermitJion(interval: Int) {
        if let action = HomeAction(characteristic: self.permitJoin, targetValue: interval) {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
}






