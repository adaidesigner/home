//
//  ServiceAirConditioner.swift
//  home
//
//  Created by Yun Zeng on 2019/2/1.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit


class ServiceAirConditioner {
    var service: Service
    
    var onChar: HMCharacteristic
    var temperatureChar: HMCharacteristic
    var modelChar: HMCharacteristic
    var rotationChar: HMCharacteristic
    var windSpeedChar: HMCharacteristic
    
    init? (service: Service) {
        if service.type != HMServiceType.airConditioner.rawValue {
            return nil
        }
        
        guard let on = service.findCharacteristic(type: .on),
            let temperature = service.findCharacteristic(type: .targetTemperature),
            let model = service.findCharacteristic(type: .airConditionerMode),
            let rotation = service.findCharacteristic(type: .airConditionerRotationMode),
            let windSpeed = service.findCharacteristic(type: .airConditionerWindSpeed) else {
                return nil
        }
        
        self.service = service
        self.onChar = on
        self.temperatureChar = temperature
        self.modelChar = model
        self.rotationChar = rotation
        self.windSpeedChar = windSpeed
    }

    // 开关
    var on: Bool {
        if let value = self.onChar.value?.toValueBool() {
            if value {
                self.service.state = .highlight
            }
            return value
        }
        return false
    }
    
    // 设置温度
    var temperature: Float {
        if let value = self.temperatureChar.value?.toValueFloat() {
            return value
        }
        return 25.0
    }
    
    // 模式
    enum Mode: Int {
        case auto = 1   // 自动
        case cool = 2   // 制冷
        case dehumidification = 3  // 除湿
        case heat = 4   // 制热
        case blowing = 5    // 送风
    }
    var mode: Mode {
        if let value = self.modelChar.value?.toValueInt() {
            if let m = Mode(rawValue: value) {
                return m
            }
        }
        return .auto
    }
    
    // 风模式
    enum RotationMode: Int {
        case natural = 1    // 自然风
        case swing = 2      // 上下摆风
    }
    var rotation: RotationMode {
        if let value = self.rotationChar.value?.toValueInt() {
            if let r = RotationMode(rawValue: value) {
                return r
            }
        }
        return .natural
    }
    
    // 风速
    var windSpeed: Int {
        if let value = self.windSpeedChar.value?.toValueInt() {
            return value
        }
        return 100
    }
}

extension ServiceAirConditioner: ServiceCellDelegate {
    var icon: UIView {
        let imageView = UIImageView(frame: ServiceCellView.iconSize)
        guard let icon = self.service.icon else {
            return imageView
        }
        
        var imgSrc = ""
        if self.service.state == .offline || self.on == false {
            imgSrc = "service-" + icon + "-off"
        } else {
            switch self.mode {
            case .auto:
                imgSrc = "service-" + icon + "-auto"
            case .blowing:
                imgSrc = "service-" + icon + "-wind"
            case .cool:
                imgSrc = "service-" + icon + "-refrigeration"
            case .dehumidification:
                imgSrc = "service-" + icon + "-dehumidification"
            case .heat:
                imgSrc = "service-" + icon + "-heat"
            }
        }
        imageView.image = UIImage(named: imgSrc)
        return imageView
    }
    
    var room: String {
        if let r = self.service.room {
            return r.name
        }
        return "默认房间"
    }
    
    var name: String {
        if self.service.name != "" {
            return self.service.name
        }
        return "请命名"
    }
    
    var state: Service.State {
        return self.service.state
    }
    
    var desc: String {
        if self.state == .offline {
            return "离线"
        }
        if self.state == .update {
            return "正在更新"
        }
        
        if self.on == false {
            return "关机"
        }
        
        var str = String(format: " %0.1f°", self.temperature)
        switch self.mode {
        case .auto:
            str = "自动" + str
        case .blowing:
            str = "送风"
        case .dehumidification:
            str = "除湿" + str
        case .cool:
            str = "制冷" + str
        case .heat:
            str = "制热" + str
        }
        return str
    }
    
    func toggle() -> HomeAction? {
        var target = 1
        if on {
            target = 0
        }
        self.service.state = .update
        let action = HomeAction(characteristic: self.onChar, targetValue: target)
        return action
    }
}

extension ServiceAirConditioner {
    func togglePower() {
        if let action = self.toggle() {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
    
    func setTemperature(value: Double) {
        if let action = HomeAction(characteristic: self.temperatureChar, targetValue: value) {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
    
    func setMode(index: Int) {
        if let action = HomeAction(characteristic: self.modelChar, targetValue: index) {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
    
    func setWindSpeed(value: Int)  {
        if let action = HomeAction(characteristic: self.windSpeedChar, targetValue: value) {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
    
    func setRotation(index: Int) {
        if let action = HomeAction(characteristic: self.rotationChar, targetValue: index) {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
    
}
