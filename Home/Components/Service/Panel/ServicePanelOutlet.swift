//
//  ServicePanelOutlet.swift
//  home
//
//  Created by Yun Zeng on 2019/9/16.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServicePanelOutlet: ServicePanelBase {
    var outlet: ServiceOutlet
    init(outlet: ServiceOutlet) {
        self.outlet = outlet
        super.init(service: outlet.service)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var iconImage: UIImageView = {
        let icon = UIImageView(frame: .zero)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.widthAnchor.constraint(equalToConstant: 240).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 200).isActive = true
        return icon
    } ()
    
    lazy var powerBtn: SwitchPowerButton = { 
        let btn = SwitchPowerButton(frame: .zero)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 125).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 300).isActive = true
        return btn
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.iconImage)
        iconImage.topAnchor.constraint(equalTo: self.customBar.bottomAnchor).isActive = true
        iconImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        self.view.addSubview(self.powerBtn)
        powerBtn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        powerBtn.topAnchor.constraint(equalTo: iconImage.bottomAnchor, constant: 85).isActive = true
        powerBtn.onPower = { power in
            if power == 1 {
                self.outlet.setPower(power: .on)
            } else {
                self.outlet.setPower(power: .off)
            }
        }
        self.updateService()
    }
    
    override func updateService() {
        if self.outlet.onState == .on { 
            self.powerBtn.setPower(on: 1)
            iconImage.image = UIImage(named: "panel-light-ceiling-on")
        } else {
            self.powerBtn.setPower(on: 0)
            iconImage.image = UIImage(named: "panel-light-ceiling-off")
        }
    }
    
}

// MARK: 开关按键
class SwitchPowerButton: UIView {
    class IconView: UIView {
        override func draw(_ rect: CGRect) {
            let strokeColor = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 7.47, y: 8))
            bezierPath.addCurve(to: CGPoint(x: 4, y: 15.76), controlPoint1: CGPoint(x: 5.35, y: 9.88), controlPoint2: CGPoint(x: 4, y: 12.66))
            bezierPath.addCurve(to: CGPoint(x: 14, y: 26), controlPoint1: CGPoint(x: 4, y: 21.42), controlPoint2: CGPoint(x: 8.48, y: 26))
            bezierPath.addCurve(to: CGPoint(x: 24, y: 15.76), controlPoint1: CGPoint(x: 19.52, y: 26), controlPoint2: CGPoint(x: 24, y: 21.42))
            bezierPath.addCurve(to: CGPoint(x: 20.55, y: 8.02), controlPoint1: CGPoint(x: 24, y: 12.67), controlPoint2: CGPoint(x: 22.66, y: 9.9))
            strokeColor.setStroke()
            bezierPath.lineWidth = 3
            bezierPath.lineCapStyle = .round
            bezierPath.lineJoinStyle = .round
            bezierPath.stroke()

            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 14, y: 3))
            bezier2Path.addLine(to: CGPoint(x: 14, y: 15.5))
            strokeColor.setStroke()
            bezier2Path.lineWidth = 3
            bezier2Path.lineCapStyle = .round
            bezier2Path.lineJoinStyle = .round
            bezier2Path.stroke()
        }
    }
    
    lazy var bgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: "EBEBEB")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 125).isActive = true
        view.heightAnchor.constraint(equalToConstant: 300).isActive = true
        view.layer.cornerRadius = 30
        return view
    } ()
    
    var posYConstraint: NSLayoutConstraint!
    
    lazy var posView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: "4F9B25")
        view.layer.cornerRadius = 23
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 105).isActive = true
        view.heightAnchor.constraint(equalToConstant: 136).isActive = true
        
        let icon = IconView(frame: .zero)
        view.addSubview(icon)
        icon.backgroundColor = UIColor.clear
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.heightAnchor.constraint(equalToConstant: 28).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        icon.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        icon.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        return view
    } ()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(bgView)
        bgView.frame = frame
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.posView)
        self.posView.centerXAnchor.constraint(equalTo: self.bgView.centerXAnchor).isActive = true
        self.posYConstraint = self.posView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10)
        self.posYConstraint.isActive = true
        
        // 捕捉下滑返回手势
        let gesture = UIPanGestureRecognizer(target: nil, action: nil)
        gesture.cancelsTouchesInView = false
        self.addGestureRecognizer(gesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func moveTo(offset: Int) {
        var y = Int(offset - 68)
        if y < 10 {
            y = 10
        } else if y > 152 {
            y = 152
        }
        self.posYConstraint.isActive = false
        self.posYConstraint = self.posView.topAnchor.constraint(equalTo: self.topAnchor, constant:  CGFloat(y))
        UIView.animate(withDuration: 0.1) {
            self.posYConstraint.isActive = true
            if y > 78 {
                self.posView.backgroundColor = UIColor(hex: "8F8F8F")
            } else {
                self.posView.backgroundColor = UIColor(hex: "4F9B25")
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.preciseLocation(in: self)
        self.moveTo(offset: Int(point.y))
        UIView.animate(withDuration: 0.1) {
            self.transform = CGAffineTransform(scaleX: 1.03, y: 1.03)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.preciseLocation(in: self)
       self.moveTo(offset: Int(point.y))
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.preciseLocation(in: self)
        var y: Int
        if point.y < 152 {
            y = 10 + 68
            self.onPower?(1)
        } else {
            y = 152 + 68
            self.onPower?(0)
        }
        self.moveTo(offset: y)
        UIView.animate(withDuration: 0.1) {
            self.transform = .identity
        }
    }
    
    // 设置开关状态
    func setPower(on: Int)  {
        var y: Int
        if on == 1 {
            y = 10 + 68
        } else {
            y = 152 + 68
        }
        self.moveTo(offset: y)
        UIView.animate(withDuration: 0.1) {
            self.transform = .identity
        }
    }
    
    // 开关状态变化
    var onPower: ((_ on: Int) -> Void)?
}






