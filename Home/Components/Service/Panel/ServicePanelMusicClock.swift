//
//  ServicePanelClock.swift
//  home
//
//  Created by Yun Zeng on 2019/9/27.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServicePanelMusicClock: ServicePanelBase {
    var clock: ServiceMusicClock
    
    init(clock: ServiceMusicClock) {
        self.clock = clock
        super.init(service: clock.service)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customBar.icon = UIImage(named: "panel-icon-music-clock")
        self.customBar.subtitle = clock.desc
        self.view.backgroundColor = UIColor(hex: "D7D7D7")
        
        guard let home = HomeManager.instance.current else {
            return
        }
        guard let automations = home.automationManager!.findAutomationsByActionCharacteristic(characteristic: clock.musicChar) else {
            return
        }
        
        // 闹钟集合
         let stackView = UIStackView()
         stackView.axis = .vertical
         stackView.distribution = .equalSpacing
         stackView.alignment = .center
         stackView.translatesAutoresizingMaskIntoConstraints = false
         self.view.addSubview(stackView)
         stackView.translatesAutoresizingMaskIntoConstraints = false
         stackView.topAnchor.constraint(equalTo: self.customBar.bottomAnchor, constant: 30).isActive = true
         stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
         stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
         stackView.customize(backgroundColor: UIColor(hex: "EBEBEB"), radiusSize: 10)
        
        for (i, automation) in automations.enumerated() {
            let action = automation.getTargetAction(characteristic: clock.musicChar)!
            let cell = ClockMusicCellView(title: automation.title, music: action.value.toString()!, action: automation.subtitle)
            stackView.addCustomControl(view: cell, height: 84, pos: (i == 0) ? .top : .middle)
        }
    
        let editBtn = UIButton()
         editBtn.setTitle("添加定时音乐闹钟", for: .normal)
         editBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
         editBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 0.6), for: .highlighted)
        
        self.view.addSubview(editBtn)

        editBtn.backgroundColor = UIColor(hex: "EBEBEB")
        editBtn.layer.cornerRadius = 10
        editBtn.translatesAutoresizingMaskIntoConstraints = false
        editBtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 14).isActive = true
        editBtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -14).isActive = true
        editBtn.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 600).isActive = true
        editBtn.heightAnchor.constraint(equalToConstant: 48).isActive = true
    }
}

// MARK: - 音乐详情
class ClockMusicCellView: UIView {
    
    init(title: String, music: String, action: String) {
        super.init(frame: .zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: 84).isActive = true
        
        let iconView = UIImageView(frame: .zero)
        self.addSubview(iconView)
        
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        iconView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        iconView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -5).isActive = true
        iconView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        iconView.image = UIImage(named: "panel-icon-music-clock")
        
        let titleLabel = UILabel()
        self.addSubview(titleLabel)
        
        var attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.medium),
        NSAttributedString.Key.kern: -0.27,
        NSAttributedString.Key.foregroundColor: UIColor.black]
        titleLabel.textAlignment = .left
        titleLabel.autoresizingMask = .flexibleWidth
        titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 66).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        
        let musicLabel = UILabel()
        self.addSubview(musicLabel)
        attributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular),
            NSAttributedString.Key.kern: 0.26,
            NSAttributedString.Key.foregroundColor: UIColor.black]
        musicLabel.textAlignment = .left
        musicLabel.autoresizingMask = .flexibleWidth
        
        musicLabel.attributedText = NSAttributedString(string: "音乐: \(music.split(separator: ".").first!)", attributes: attributes)
        
        musicLabel.translatesAutoresizingMaskIntoConstraints = false
        musicLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 66).isActive = true
        musicLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        musicLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let actionLabel = UILabel()
        self.addSubview(actionLabel)
        attributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular),
            NSAttributedString.Key.kern: 0.26,
            NSAttributedString.Key.foregroundColor: UIColor(hex: "858585")]
        actionLabel.textAlignment = .left
        actionLabel.autoresizingMask = .flexibleWidth
        actionLabel.attributedText = NSAttributedString(string: action, attributes: attributes)
        
        actionLabel.translatesAutoresizingMaskIntoConstraints = false
        actionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 66).isActive = true
        actionLabel.topAnchor.constraint(equalTo: musicLabel.bottomAnchor).isActive = true
        actionLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        let arrow = UIImageView(frame: .zero)
        self.addSubview(arrow)
        
        arrow.translatesAutoresizingMaskIntoConstraints = false
        arrow.heightAnchor.constraint(equalToConstant: 44).isActive = true
        arrow.widthAnchor.constraint(equalToConstant: 44).isActive = true
        arrow.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        arrow.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
        arrow.image = UIImage(named: "panel-music-clock-detail-icon")
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}







