//
//  ServiceControlPanel.swift
//  home
// 设备详情操作页面
//
//  Created by Yun Zeng on 2019/8/7.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServicePanelBase: UIViewController {
    lazy var customBar: CustomNavigationBar = {
        let bar = CustomNavigationBar()
        return bar
    } ()

    var onBack: (()->Swift.Void)?
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        self.onBack?()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.onBack?()
    }
    
    var service: Service
    init(service: Service) {
        self.service = service
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateService() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        view.addSubview(self.customBar)
        
        customBar.title = self.service.name
        if service.state == .offline {
            customBar.subtitle = "设备离线"
        } else {
            customBar.subtitle = "设备在线"
        }
        
        if let icon = service.icon {
            customBar.icon = UIImage(named: "panel-icon-" + icon)
        }
        
        customBar.translatesAutoresizingMaskIntoConstraints = false
        customBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        customBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        customBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        customBar.cancel = {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension ServicePanelBase {
    static func getCustomPanel(cell: ServiceCellDelegate) -> ServicePanelBase {
        var panel: ServicePanelBase
        if let light = cell as? ServiceLightHue {
            panel = ServicePanelLightHue(light: light)
        } else if let outlet = cell as? ServiceOutlet {
            panel = ServicePanelOutlet(outlet: outlet)
        } else if let air = cell as? ServiceAirConditioner {
            panel = ServicePanelAirConditioner(air: air)
        } else if let htSensor = cell as? ServiceHTSensor {
            panel = ServicePanelHTSensor(sensor: htSensor)
        } else if let button = cell as? ServiceSceneButton {
            panel = ServicePanelSceneButton(button: button)
        } else if let contact = cell as? ServiceSensorContact {
            panel = ServicePanelSensorContact(contact: contact)
        } else if let coordinator = cell as? ServiceCoordinator {
            panel = ServicePanelCoordinator(coordinator: coordinator)
        } else if let motion = cell as? ServiceSensorMotion {
            panel = ServicePanelSensorMotion(motion: motion)
        } else if let clock = cell as? ServiceMusicClock {
            panel = ServicePanelMusicClock(clock: clock)
        } else if let button = cell as? ServiceSceneButton {
            panel = ServicePanelSceneButton(button: button)
        } else if let light = cell as? ServiceLightColorTemperature {
            panel = ServicePanelLightColorTemperature(light: light)
        } else {
            panel = ServicePanelBase(service: cell.service)
        }
        return panel
    }
}





