//
//  UserCostomBar.swift
//  home
//
//  Created by Yun Zeng on 2019/8/6.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class UserCustomBarView: UIView {
    
    public var opacity: Float = 0 {
        didSet {
            if opacity > 1.0 {
                opacity = 1.0
            }
            self.blurView.layer.opacity = opacity
            if opacity > 0.95 {
                self.titleLabel.layer.opacity = 1
                self.headView.layer.opacity = 1
            } else if opacity < 0.9 {
                self.titleLabel.layer.opacity = 0
                self.headView.layer.opacity = 0
            }
        }
    }
    
    private lazy var blurView: UIVisualEffectView = {
        let blur =  UIVisualEffectView()
        blur.effect = UIBlurEffect(style: .light)
        blur.translatesAutoresizingMaskIntoConstraints = false
        blur.clipsToBounds = true
        blur.layer.opacity = 0
        return blur
    }()
    
    private lazy var editImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "user-edit")
        return view
    }()
    
    public var title: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold),
                NSAttributedString.Key.kern: -0.41,
                NSAttributedString.Key.foregroundColor: UIColor.black]
            self.titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
        }
    }
    
    fileprivate lazy var headView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "user-head")
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.semibold),
            NSAttributedString.Key.kern: -0.41,
            NSAttributedString.Key.foregroundColor: UIColor.black]
        label.attributedText = NSAttributedString(string: self.title, attributes: attributes)
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    func initialize() {
        self.backgroundColor = UIColor.clear
        self.addSubview(self.blurView)
        self.addSubview(self.editImage)
        self.addSubview(self.titleLabel)
        self.addSubview(self.headView)
        
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        blurView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        blurView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        blurView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 14).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
        titleLabel.autoresizingMask = .flexibleWidth
        
        headView.translatesAutoresizingMaskIntoConstraints = false
        headView.bottomAnchor.constraint(equalTo: bottomAnchor, constant:  -11).isActive = true
        headView.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: -10).isActive = true
        headView.widthAnchor.constraint(equalToConstant: 28).isActive = true
        headView.heightAnchor.constraint(equalToConstant: 28).isActive = true
        headView.layer.cornerRadius = 14
        headView.layer.masksToBounds = true
        headView.clipsToBounds = true
        
        editImage.translatesAutoresizingMaskIntoConstraints = false
        editImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        editImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -18).isActive = true
        editImage.widthAnchor.constraint(equalToConstant: 32).isActive = true
        editImage.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        self.headView.layer.opacity = 0
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}
