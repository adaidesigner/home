//
//  UserFeedbackController.swift
//  home
//
//  Created by Yun Zeng on 2019/9/29.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class UserFeedbackController: UIViewController {
    
    private lazy var customBar: CustomNavigationBar = {
        let bar = CustomNavigationBar()
        bar.title = "意见反馈"
        bar.subtitle = Date().toString(format: "YYYY-MM-dd")
        bar.icon = UIImage(named: "user-feedback")
        return bar
    } ()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                self.view.backgroundColor = UIColor(hex: "dedede")
        
        view.addSubview(self.customBar)
        customBar.translatesAutoresizingMaskIntoConstraints = false
        customBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        customBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        customBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        customBar.cancel = {
            self.dismiss(animated: true, completion: nil)
        }
    }
}









