//
//  TabController.swift
//  home
//
//  Created by Yun Zeng on 2019/9/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class TabController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = UIColor(hex: "FF9500")
    }
}

