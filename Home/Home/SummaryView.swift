//
//  SummaryView.swift
//  home
//
//  Created by Yun Zeng on 2019/8/5.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

import UIKit

class SummaryView: UIView {
    
    var home: String = "云の家" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 36, weight: UIFont.Weight.medium),
                NSAttributedString.Key.kern: 0.39,
                NSAttributedString.Key.foregroundColor: UIColor.white]
            self.homeLabel.attributedText = NSAttributedString(string: self.home, attributes: attributes)
        }
    }
    
    var devsInfo: String = "家中有4台设备h处于打开状态" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium),
                NSAttributedString.Key.kern: 0.29,
                NSAttributedString.Key.foregroundColor: UIColor.white]
            self.devsInfoLabel.attributedText = NSAttributedString(string: self.devsInfo, attributes: attributes)
        }
    }
    
    var envInfo: String = "卧室温度 25.5° 湿度 67% 光照 96lux" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium),
                NSAttributedString.Key.kern: 0.29,
                NSAttributedString.Key.foregroundColor: UIColor.white]
            self.envInfoLabel.attributedText = NSAttributedString(string: self.envInfo, attributes: attributes)
        }
    }
    
    private lazy var homeLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var devsInfoLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var envInfoLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var envGraphBtn: UIButton = {
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium),
            NSAttributedString.Key.kern: 0.29,
            NSAttributedString.Key.foregroundColor: UIColor.white,
            ]
        
        let button = UIButton()
        button.contentHorizontalAlignment = .left
        button.setAttributedTitle(NSAttributedString(string: "环境变化曲线 >>", attributes: attributes), for: UIControl.State.normal)
        return button
    }()
    
    var height: Int {
        get {
            return Int(self.envGraphBtn.frame.maxY + self.envGraphBtn.frame.height)
        }
    }
    
    func initialize() {
        self.addSubview(self.homeLabel)
        
        let margin: CGFloat = 20.0
        let marginTop: CGFloat = 5.0
        
        homeLabel.translatesAutoresizingMaskIntoConstraints = false
        homeLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        homeLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: margin).isActive = true
        homeLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: margin).isActive = true
        
        self.addSubview(self.devsInfoLabel)
        
        devsInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        devsInfoLabel.topAnchor.constraint(equalTo: self.homeLabel.bottomAnchor, constant: marginTop).isActive = true
        devsInfoLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: margin).isActive = true
        devsInfoLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: margin).isActive = true
        
        self.addSubview(self.envInfoLabel)
        
        envInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        envInfoLabel.topAnchor.constraint(equalTo: self.devsInfoLabel.bottomAnchor, constant: marginTop).isActive = true
        envInfoLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: margin).isActive = true
        envInfoLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: margin).isActive = true
        
        self.addSubview(self.envGraphBtn)
        
        envGraphBtn.translatesAutoresizingMaskIntoConstraints = false
        envGraphBtn.topAnchor.constraint(equalTo: self.envInfoLabel.bottomAnchor, constant: 0).isActive = true
        envGraphBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: margin).isActive = true
        envGraphBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: margin).isActive = true
        envGraphBtn.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        self.backgroundColor = UIColor.clear
        
        // shadow
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 1.0
    }
    
    func updateSummary() {
        if CloudClient.instance.state == .disconnect {
            self.devsInfo = "正在连接网络..."
            self.envGraphBtn.isHidden = true
            self.envInfo = ""
            return
        }
        
        let count = self.manager.serviceOpenedCount
        if count != 0 {
            self.devsInfo = "家中有\(count)台设备开着"
        } else {
            self.devsInfo = "所有设备都已经关闭"
        }
        
        var envStr = ""
        if let temperature = self.manager.temperature {
            envStr = String(format: "家中温度 %0.1f° ", temperature)
        }
        if let humidity = self.manager.humidity {
            envStr += String(format: "湿度 %d%% ", humidity)
        }
        if let ambientLight = self.manager.ambientLight {
            envStr += String(format: "光照强度 %dlux", ambientLight)
        }
        self.envInfo = envStr
        if envStr != "" {
            self.envGraphBtn.isHidden = false
        }
    }
    
    var manager: SummaryModel
    init(manager: ServiceManager) {
        self.manager = SummaryModel(manager: manager)
        super.init(frame: CGRect())
        
        initialize()
        updateSummary()
        self.manager.onUpdate = self.updateSummary
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
