//
//  SceneCellDelegate.swift
//  home
//
//  Created by Yun Zeng on 2019/1/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

protocol SceneCellDelegate {
    var name: String {get}
    var stateIcon: String {get}
    var state: SceneState {get}
    var subtitle: String {get}
    
    // 返回值: 是否需要刷新显示
    func checkActionsExected(home: Home) -> Bool
    func execute(home: Home) -> Bool
}

// 显示内容
extension HomeScene: SceneCellDelegate {
    var stateIcon: String {
        if self.state != .idle {
            return self.icon + "-on"
        } else {
            return self.icon + "-off"
        }
    }
    
    var subtitle: String {
        if self.state == .executing {
            return "正在执行"
        } else {
            return ""
        }
    }
    
    func checkActionsExected(home: Home) -> Bool {
        return false
    }
    
    func execute(home: Home) -> Bool {
        return false
    }
}
