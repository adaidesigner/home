//
//  ServiceCell.swift
//  home
//
//  Created by Yun Zeng on 2019/1/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit


class ServiceCell: UICollectionViewCell {
    static let identifier = "serviceCellViewIdentifier"
    
    var serviceView: ServiceCellView!
    
    // 压力点击跳转时间
    var action: ((_ service: ServiceCell) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.serviceView = ServiceCellView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        self.addSubview(self.serviceView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // 设置信息
    func setService(service: ServiceCellDelegate, action: ((_ service: ServiceCell) -> Void)? = nil) {
        if service.name != self.serviceView.name {
            self.serviceView.name = service.name
        }
        if service.room != self.serviceView.room {
            self.serviceView.room = service.room
        }
        if service.icon != self.serviceView.iconView {
            self.serviceView.iconView = service.icon
        }
        if service.desc != self.serviceView.desc {
            self.serviceView.desc = service.desc
        }
        if service.state != self.serviceView.state {
            self.serviceView.state = service.state
        }
        self.action = action
    }
    
    // 重按跳转至详情页面动画部分
    fileprivate var feedbackThreshold: Double = 0.1
    var isPlayed: Bool = false {
        didSet {
            if self.isPlayed == false {
                UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
                    self.transform = .identity
                })
            }
        }
    }
    
    lazy var generator: UIImpactFeedbackGenerator = {
        return UIImpactFeedbackGenerator(style: .light)
    }()
    
    public func setStatic() {
        if self.bTouched {
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
                self.transform = .identity
            })
        }
        if self.isPlayed {
            self.isPlayed = false
        }
    }
    
    private var bTouched = false
    // 压力感应
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.bTouched = true
//        if traitCollection.forceTouchCapability == .available {
            self.generator.prepare()
            UIView.animate(withDuration: 0.1) {
                self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
//            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if traitCollection.forceTouchCapability == .available {
            if let touch = touches.first {
                let rate = touch.force / touch.maximumPossibleForce
                let scaling = 1.0 + (rate / 2) + 0.3
                UIView.animate(withDuration: 0.1, animations: {
                    self.transform = CGAffineTransform(scaleX: scaling, y: scaling)
                })
                
                if Double(rate) >= self.feedbackThreshold {
                    if !self.isPlayed {
                        self.isPlayed = true
                        self.action?(self)
                    }
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.bTouched = false
        // 当服务图标进入详情控制页面时，停止还原动画
        // 等待从详情页面返回，再还原图标大小
        if self.isPlayed == false {
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
                self.transform = .identity
            })
        }
    }
}
