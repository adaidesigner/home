//
//  ServiceCellDelegate.swift
//  home
//
//  Created by Yun Zeng on 2019/1/31.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit


protocol ServiceCellDelegate {
    var service: Service {get}  // 关联服务
    
    var icon: UIView {get}      // 显示图标
    
    var room: String {get}      // 房间
    var name: String {get}      // 服务名
    var state: Service.State {get}  // 状态
    var desc: String {get}
    
    func toggle() -> HomeAction?    // 点击控制事件
}




