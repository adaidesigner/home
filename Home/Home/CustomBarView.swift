//
//  CustomBarView.swift
//  home
//
//  Created by Yun Zeng on 2019/8/5.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class CustomBarView: UIView {

    public var opacity: Float = 0.0 {
        didSet {
            self.blurView.layer.opacity = opacity
            if opacity > 0.95 {
                self._title = self.title
            } else if opacity < 0.9 {
                self._title = ""
            }
        }
    }
    
    private lazy var blurView: UIVisualEffectView = {
        let blur =  UIVisualEffectView()
        blur.effect = UIBlurEffect(style: .dark)
        blur.translatesAutoresizingMaskIntoConstraints = false
        blur.clipsToBounds = true
        blur.layer.opacity = 0
        return blur
    }()
    
    
    private lazy var locationImage: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 13
        view.image = UIImage(named: "location")
        return view
    }()
    
    private lazy var addImage: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 13
        view.image = UIImage(named: "add")
        return view
    }()
    
    private var _title: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold),
                NSAttributedString.Key.kern: -0.41,
                NSAttributedString.Key.foregroundColor: UIColor.white]
            self.titleLabel.attributedText = NSAttributedString(string: self._title, attributes: attributes)
        }
    }
    
    public var title: String = "我的家" {
        didSet {
            if opacity > 0.95 {
                self._title = self.title
            } else if opacity < 0.9 {
                self._title = ""
            }
        }
    }
    
    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold),
            NSAttributedString.Key.kern: -0.41,
            NSAttributedString.Key.foregroundColor: UIColor.white]
        label.attributedText = NSAttributedString(string: self.title, attributes: attributes)
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    func initialize() {
        self.backgroundColor = UIColor.clear
        
        self.addSubview(self.blurView)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        blurView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        blurView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        blurView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        
        self.addSubview(self.addImage)
        addImage.translatesAutoresizingMaskIntoConstraints = false
        addImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -18).isActive = true
        addImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        addImage.heightAnchor.constraint(equalToConstant: 26).isActive = true
        addImage.widthAnchor.constraint(equalToConstant: 26).isActive = true

        self.addSubview(self.locationImage)
        locationImage.translatesAutoresizingMaskIntoConstraints = false
        locationImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 18).isActive = true
        locationImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        locationImage.heightAnchor.constraint(equalToConstant: 26).isActive = true
        locationImage.widthAnchor.constraint(equalToConstant: 26).isActive = true
        
        self.addSubview(self.titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Logger.Info("custom bar frame: \(self.frame.debugDescription)")
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}
