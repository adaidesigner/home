//
//  SummaryModel.swift
//  home
//
//  Created by Yun Zeng on 2019/8/5.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

// 家庭摘要信息
class SummaryModel: ServiceUpdateObserver {
    static let identify = "home.controller.summary"
    
    var manager: ServiceManager
    init (manager: ServiceManager) {
        self.manager = manager
        manager.addObserver(identify: SummaryModel.identify, observer: self)
    }
    
    func setServiceManager(manager: ServiceManager) {
        self.manager = manager
        manager.addObserver(identify: SummaryModel.identify, observer: self)
    }
    
    var onUpdate: (()->Void)?
    func update(services: Set<Service>) {
        self.onUpdate?()
    }
    
    // 获取家里所有传感器的平均湿度值
    var temperature: Float? {
        get {
            guard let services = manager.findService(type: .htSensor) else {
                return nil
            }
            var temp: Float = 0.0
            var count = 0
            for service in services {
                if let t = service.getCharacteristicValue(type: .currentTemperature)?.toValueFloat() {
                    temp = temp + t
                    count = count + 1
                }
            }
            if count == 0 {
                return nil
            }
            return temp / Float(count)
        }
    }
    
    // 获取家里所有传感器的平均湿度
    var humidity: Int? {
        get {
            guard let services = manager.findService(type: .htSensor) else {
                return nil
            }
            var temp = 0
            var count = 0
            for service in services {
                if let t = service.getCharacteristicValue(type: .currentRelativeHumidity)?.toValueInt() {
                    temp = temp + t
                    count = count + 1
                }
            }
            if count == 0 {
                return nil
            }
            return temp / count
        }
    }
    
    // 家庭所有传感器的平均光照强度
    var ambientLight: Int? {
        get {
            guard let services = manager.findService(type: .lightSensor) else {
                return nil
            }
            var temp = 0
            var count = 0
            for service in services {
                if let t = service.getCharacteristicValue(type: .currentAmbientLightLevel)?.toValueInt() {
                    temp = temp + t
                    count = count + 1
                }
            }
            if count == 0 {
                return nil
            }
            return temp / count
        }
    }
    
    // 设备打开的状态是指, 包含属性为.on的所有属性的服务
    var serviceOpenedCount: Int {
        get {
            guard let services = manager.findServiceContainCharacteristicType(type: .on) else {
                return 0
            }
            var count = 0
            for service in services {
                if let opened = service.getCharacteristicValue(type: .on)?.toValueBool(), opened {
                    if service.state != .offline {
                        count = count + 1
                    }
                }
            }
            return count
        }
    }
}

// 服务管理功能推展
extension ServiceManager {
    // 获取指定类型的服务
    func findService(type: HMServiceType) -> [Service]? {
        var services = [Service]()
        for sev in self.services {
            if sev.type == type.rawValue {
                services.append(sev)
            }
        }
        if services.count == 0 {
            return nil
        }
        return services
    }
    
    // 包含指定属性类型的服务
    func findServiceContainCharacteristicType(type: HMCharacteristicType) -> [Service]? {
        var services = [Service]()
        for sev in self.services {
            if sev.findCharacteristic(type: type) != nil {
                services.append(sev)
            }
        }
        if services.count == 0 {
            return nil
        }
        return services
    }
}

// 服务功能扩展
extension Service {
    // 获取指定类型的属性
    func getCharacteristicValue(type: HMCharacteristicType) -> AnyJSONType? {
        guard let service = self.instance else {
            return nil
        }
        for characteristic in service.characteristics {
            if characteristic.type == type {
                return characteristic.value
            }
        }
        return nil
    }
}
