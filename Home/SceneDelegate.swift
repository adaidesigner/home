//
//  SceneDelegate.swift
//  home
//
//  Created by Yun Zeng on 2019/8/4.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    override init() {
        super.init()
        Account.instance.addObserver(identify: "adai.design.home", observer: self)
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        if !Account.instance.enable {
            self.showLoginController()
        }
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }
}

extension SceneDelegate: AccountDelegate {
    func showLoginController() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let viewController = storyboard.instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController else {
                return
            }
            viewController.modalPresentationStyle = .formSheet
            viewController.isModalInPresentation = true
            self.window?.rootViewController?.present(viewController, animated: true, completion: nil)
        }
    }
    
    func AccountDidLogin() {
    }
    
    func AccountDidLogout(state: String) {
        Logger.Info("Logout: \(state)")
        self.showLoginController()
    }
}


